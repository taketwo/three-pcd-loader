/* eslint-env mocha */

var expect = require('chai').expect
var fs = require('fs')
var product = require('map-cartesian-product')
var given = require('mocha-testdata')
var PCDLoader = require('../pcd-loader')

function readTestFile (name) {
  var data = fs.readFileSync('test/data/' + name + '.pcd')
  return data.buffer.slice(data.byteOffset, data.byteOffset + data.byteLength)
}

var modes = ['ascii', 'binary', 'binary_compressed']
var colors = ['rgb', 'rgba']

describe('PCDLoader', function () {
  it('is an object and has load() and parse() methods', function () {
    var loader = new PCDLoader()
    expect(loader).to.be.an('object')
    expect(loader).to.respondTo('load')
    expect(loader).to.respondTo('parse')
  })

  given(modes).it('can parse a PCD file with XYZ fields', function (mode) {
    var loader = new PCDLoader()
    var data = readTestFile('xyz_' + mode)
    var mesh = loader.parse(data)
    // Properties
    expect(mesh).to.have.property('header')
    expect(mesh).to.have.property('geometry')
    expect(mesh).to.have.property('material')
    // Header
    expect(mesh.header.width).to.equal(100)
    expect(mesh.header.height).to.equal(1)
    expect(mesh.header.points).to.equal(100)
    expect(mesh.header.fields).to.have.length(3)
    expect(mesh.header.size).to.have.length(3)
    expect(mesh.header.type).to.have.length(3)
    expect(mesh.header.count).to.have.length(3)
    expect(mesh.header.data).to.equal(mode)
    // Attributes
    expect(mesh.geometry.getAttribute('position')).to.exist
    expect(mesh.geometry.getAttribute('color')).to.not.exist
    // Position
    var position = mesh.geometry.getAttribute('position')
    expect(position).to.have.property('count', 100)
    expect(position).to.have.property('itemSize', 3)
    for (var i = 0; i < 100; i++) {
      expect(position.array[i * 3 + 0]).to.closeTo(i, 0.000001)
      expect(position.array[i * 3 + 1]).to.closeTo(100 - i, 0.000001)
      expect(position.array[i * 3 + 2]).to.closeTo(0, 0.000001)
    }
  })

  given(product({mode: modes, color: colors})).it('can parse a PCD file with XYZ and RGB/RGBA fields', function (ctx) {
    var loader = new PCDLoader()
    var data = readTestFile('xyz' + ctx.color + '_' + ctx.mode)
    var mesh = loader.parse(data)
    // Properties
    expect(mesh).to.have.property('header')
    expect(mesh).to.have.property('geometry')
    expect(mesh).to.have.property('material')
    // Header
    expect(mesh.header.width).to.equal(5)
    expect(mesh.header.height).to.equal(2)
    expect(mesh.header.points).to.equal(10)
    expect(mesh.header.fields).to.have.length(4)
    expect(mesh.header.size).to.have.length(4)
    expect(mesh.header.type).to.have.length(4)
    expect(mesh.header.count).to.have.length(4)
    expect(mesh.header.data).to.equal(ctx.mode)
    // Attributes
    expect(mesh.geometry.getAttribute('position')).to.exist
    expect(mesh.geometry.getAttribute('color')).to.exist
    var position = mesh.geometry.getAttribute('position')
    expect(position).to.have.property('count', 10)
    expect(position).to.have.property('itemSize', 3)
    var color = mesh.geometry.getAttribute('color')
    expect(color).to.have.property('count', 10)
    expect(color).to.have.property('itemSize', 3)
    var id = 0
    for (var j = 0; j < 2; j++) {
      for (var i = 0; i < 5; i++, id++) {
        expect(position.array[id * 3 + 0]).to.closeTo(i, 0.000001)
        expect(position.array[id * 3 + 1]).to.closeTo(j, 0.000001)
        expect(position.array[id * 3 + 2]).to.closeTo(0, 0.000001)
        expect(color.array[id * 3 + 0]).to.closeTo(1.0 / 255.0 * 0xFF, 0.000001)
        expect(color.array[id * 3 + 1]).to.closeTo(1.0 / 255.0 * 0x7F, 0.000001)
        expect(color.array[id * 3 + 2]).to.closeTo(1.0 / 255.0 * id, 0.000001)
      }
    }
  })
})
