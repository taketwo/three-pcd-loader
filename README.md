A THREE loader for PCD files.

Based on the example [THREE.PCDLoader](http://threejs.org/examples/#webgl_loader_pcd) written by Filipe Caixeta.

Changes:
  * added support for compressed binary files
  * significantly improved header parsing time
  * added support for RGBA color field
  * removed support for normals field
